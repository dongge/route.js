//前端路由 本来打算写成一个函数，但路由对象只需要全局存在一个即可 并没有发现需要多个对象存在的场景
var route=(function(){
	var p={
		intes:[]
	};
	function getPath(url){
		var path=url.split("#")[1];
		if(!path)return "/";
		if(path.charAt(0)!="/")path="/"+path;
		return path;
	}
	function use(fn){
		p.intes.push(fn);
	}
	function hashchange(path){
		var req={path:path},hlen=p.intes.length;
		if(hlen==0){
			doother(req);
			return;
		}
		//执行拦截器链
		!function intec(i){
			if(i==hlen){
				return;
			}
			p.intes[i](req,function(){
				intec(i+1);
			});
		}(0);
	}
	function to(path){
		window.location.hash=path;
	}
	function start(){
		window.onhashchange=function(){
			var path=getPath(location.href);
			hashchange(path);
		}
		var path=getPath(location.href);
		hashchange(path);
	}
	var result={
		start:start,
		use:use,
		to:to,
		mix:mix
	}
	//扩展API
	function mix(obj){
		for(var key in obj){
			result[key]=obj[key];
		}
	}
	return result;
})();